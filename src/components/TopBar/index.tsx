import {FC} from 'react';
import styles from './TopBar.module.scss';

const TopBar: FC = () => {
  return (
    <div className={styles.root}>
      <h2>Contacts</h2>
    </div>
  );
};

export default TopBar;
