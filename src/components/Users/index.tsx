import React, {FC, useState, useLayoutEffect} from 'react';
import useFetch from '../../hooks/useFetch';
import stylesList from './style/List.module.scss';
import stylesContainer from './style/Container.module.scss';
import Avatar from '../Avatar';

export interface IUser {
  avatar: string;
  email: string;
  first_name: string;
  gender: string;
  id: number;
  last_name: string;
}

const Users: FC = () => {
  const [search, setSearch] = useState('');
  const [checkedInfo, setCheckedInfo] = useState<any>({});

  // Import the users data
  const {data: users, loading} = useFetch<IUser[] | null>(
    'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json',
  );

  // Sort the list by last_name (alphabetically)
  const sortedUsers = users?.sort((a, b) =>
    a?.last_name > b?.last_name ? 1 : -1,
  );

  // Search by first_name and last_name
  const filteredUsers = sortedUsers?.filter((user) => {
    return (
      user?.first_name.toLowerCase().includes(search?.toLowerCase()) ||
      user?.last_name.toLowerCase().includes(search?.toLowerCase())
    );
  });

  const handleSearch = (e: any) => {
    if (!loading) {
      setSearch(e.target.value);
    }
  };

  const resultUserList = () => {
    if (filteredUsers?.length === 0) {
      return <div className={stylesContainer.staff}>No result</div>;
    }

    // Handler a checkbox click by user id
    const toggleChecked = (item: IUser) => () => {
      setCheckedInfo((state: IUser[]) => ({
        ...state,
        [item.id]: state[item.id] ? null : item.id,
      }));
    };

    const result = filteredUsers?.map((user) => (
      <li key={user.id} className={stylesList.item}>
        <Avatar
          image={user.avatar}
          firstName={user.first_name}
          lastName={user.last_name}
        />
        {user?.first_name} {user?.last_name}
        <input
          className={stylesList.checkbox}
          name={user?.first_name}
          value=''
          onChange={toggleChecked(user)}
          checked={!!checkedInfo[user.id]}
          type='checkbox'
        />
      </li>
    ));

    return result;
  };

  // Show all selected contacts in console
  useLayoutEffect(() => {
    console.log(
      "Contacts ID's:",
      Object.keys(checkedInfo).filter((c) => checkedInfo[c]),
    );
  }, [checkedInfo]);

  return (
    <form className={stylesContainer.root}>
      <div className={stylesContainer.input}>
        <input type='search' placeholder='Search' onChange={handleSearch} />
      </div>
      {loading && <div className={stylesContainer.staff}>Loading...</div>}
      {!loading && <ul className={stylesList.list}>{resultUserList()}</ul>}
    </form>
  );
};

export default React.memo(Users);
