import {FC} from 'react';
import styles from './Avatar.module.scss';

interface IAvatar {
  image: string;
  firstName?: string;
  lastName?: string;
}

const getShortName = (firstName?: string, lastName?: string) => {
  if (!firstName && !lastName) {
    return '--';
  }
  const name = firstName?.charAt(0).toUpperCase();
  const surname = lastName?.charAt(0).toUpperCase();

  return `${name}${surname}`;
};

const Avatar: FC<IAvatar> = ({image = '', firstName = '', lastName = ''}) => {
  return (
    <span className={styles.root}>
      {image && <img className={styles.image} src={image} alt='' />}
      {!image && (
        <span className={styles.name}>{getShortName(firstName, lastName)}</span>
      )}
    </span>
  );
};

export default Avatar;
