import {useEffect, useState} from 'react';
import axios from 'axios';

function useFetch<T>(url: string): {
  data: T | null;
  error?: Error | null;
  loading?: boolean;
} {
  const [data, setData] = useState<T | null>(null);
  const [error, setError] = useState<Error | null>(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios
      .get(url)
      .then((res) => {
        setData(res.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log('Error', err);
        setLoading(false);
        setError(err);
      })
      .finally(() => setLoading(false));
  }, [url]);

  return {data, error, loading};
}

export default useFetch;
