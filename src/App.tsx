import {FC} from 'react';
import styles from './App.module.scss';
import TopBar from './components/TopBar';
import Users from './components/Users';

const App: FC = () => {
  return (
    <div className={styles.app}>
      <TopBar />
      <Users />
    </div>
  );
};

export default App;
